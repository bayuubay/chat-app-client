import React, { useState, createRef } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

import "./Join.css";
const Join = () => {
  const usernameRef = createRef();
  const roomRef = createRef();
    const chatLink=(event) => (!name || !room ? event.preventDefault() : null)
  
    const joinChat = () => {
    const username = usernameRef.current.value;
    const roomId = roomRef.current.value;

    axios
      .post("http://localhost:8000/join", {
        username,
        roomId,
      })
      .then((response) => {
        alert(response.data.message);
      
      })
      .catch((error) => console.log(error));
  };

  const [name, setName] = useState("");
  const [room, setRoom] = useState("");
  // console.log(setRoom(event.target.value))
  return (
    <div className="joinOuterContainer">
      <div className="joinInnerContainer">
        <h1 className="heading">Join Chatroom</h1>
        <div>
          <input
            placeholder="Username"
            className="joinInput"
            type="text"
            ref={usernameRef}
            onChange={(event) => setName(event.target.value)}
          />
        </div>
        <div>
          <input
            placeholder="RoomID"
            className="joinInput mt-20"
            type="text"
            ref={roomRef}
            onChange={(event) => setRoom(event.target.value)}
          />
        </div>
        <Link
          onClick={
              chatLink,
              joinChat
            }
          to={`/chat?name=${name}&room=${room}`}
        >
          <button className="button mt-21" type="submit">
            Join
          </button>
        </Link>
      </div>
    </div>
  );
};

export default Join;
